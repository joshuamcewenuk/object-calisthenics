import { ItemsCollection, Currency, Item, Sku } from "../../models";

describe("CheckoutItems", () => {
  it("Returns total of Items correctly.", () => {
    const items = new ItemsCollection();
    items.addItems([
      new Item(new Currency(100), new Sku("ABC")),
      new Item(new Currency(200), new Sku("DEF")),
    ]);

    expect(items.getTotal()).toEqual(new Currency(300));
  });
});
