import { Sku } from "../../models";

describe("Sku", () => {
  it("Returns the value of SKU.", () => {
    const sku = new Sku("ABC");

    expect(sku.getValue()).toEqual("ABC");
  });
});
