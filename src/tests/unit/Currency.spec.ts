import { Currency } from "../../models";

describe("Currency", () => {
  it("Returns the numeric value of Currency.", () => {
    const currency = new Currency(100);

    expect(currency.getAmount()).toEqual(100);
  });

  it("Returns formatted representation of Currency.", () => {
    const currency = new Currency(100);

    expect(currency.toString()).toEqual("£1.00");
  });
});
