import { Currency, Item, Sku } from "../../models";

describe("Item", () => {
  it("Returns the price of Item.", () => {
    const price = new Currency(100);
    const item = new Item(price, new Sku("ABC"));

    expect(item.getPrice()).toEqual(price);
  });

  it("Returns the SKU of Item.", () => {
    const sku = new Sku("ABC");
    const item = new Item(new Currency(100), sku);

    expect(item.getSku()).toEqual(sku);
  });
});
