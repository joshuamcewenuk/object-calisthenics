import { Checkout, Currency, Item, Sku } from "../../models";

describe("Checkout", () => {
  it("Returns total price of 0 when no Items have been scanned.", () => {
    const checkout = new Checkout();

    expect(checkout.getTotal()).toEqual("£0.00");
  });

  it("Returns total price of checkout when a single Item is scanned.", () => {
    const item = new Item(new Currency(10000), new Sku("ABC"));

    const checkout = new Checkout();
    checkout.scan(item);

    expect(checkout.getTotal()).toEqual("£100.00");
  });

  it("Returns total price of checkout when multiple Items are scanned.", () => {
    const item1 = new Item(new Currency(10000), new Sku("ABC"));
    const item2 = new Item(new Currency(900), new Sku("DEF"));

    const checkout = new Checkout();
    checkout.scan(item1);
    checkout.scan(item2);

    expect(checkout.getTotal()).toEqual("£109.00");
  });
});
