import { Item } from "./Item";
import { ItemsCollection } from "./ItemsCollection";

export class Checkout {
  private items: ItemsCollection = new ItemsCollection();

  /**
   * Scans an Item.
   * @param item
   */
  public scan(item: Item): void {
    this.items.addItem(item);
  }

  /**
   * Retrieves total of scanned Items.
   */
  public getTotal(): string {
    const total = this.items.getTotal();
    return total.toString();
  }
}
