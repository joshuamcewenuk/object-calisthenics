/**
 * Representation of a Currency value.
 */
export class Currency {
  private readonly amount: number;

  /**
   * Create a new Currency instance.
   * @param amount
   */
  constructor(amount: number) {
    this.amount = amount;
  }

  /**
   * Retrieve numeric value for Currency.
   */
  public getAmount(): number {
    return this.amount;
  }

  /**
   * Return human-readable representation of Currency.
   */
  public toString(): string {
    const amount = this.getAmount();
    const formatter = new Intl.NumberFormat("en-GB", {
      style: "currency",
      currency: "GBP",
    });

    return formatter.format(amount / 100);
  }
}
