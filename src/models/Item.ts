import { Currency } from "./Currency";
import { Sku } from "./Sku";

/**
 * Representation of an Item.
 */
export class Item {
  private readonly price: Currency;
  private readonly sku: Sku;

  /**
   * Create a new Item instance.
   * @param price
   * @param sku
   */
  constructor(price: Currency, sku: Sku) {
    this.price = price;
    this.sku = sku;
  }

  /**
   * Retrieve the Price of the Item.
   */
  public getPrice(): Currency {
    return this.price;
  }

  /**
   * Retrieve the SKU of the Item.
   */
  public getSku(): Sku {
    return this.sku;
  }
}
