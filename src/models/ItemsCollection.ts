import { Item } from "./Item";
import { Currency } from "./Currency";

/**
 * Representation of collection of Items.
 */
export class ItemsCollection {
  private items: Item[] = [] as Item[];

  /**
   * Adds an Item to the collection.
   * @param item
   */
  public addItem(item: Item): void {
    this.items.push(item);
  }

  /**
   * Adds Items to the collection.
   * @param items
   */
  public addItems(items: Item[]): void {
    this.items = [...this.items, ...items];
  }

  /**
   * Sum all Items in the collection.
   */
  public getTotal(): Currency {
    const total = this.items.reduce((accumulator, item) => {
      const price = item.getPrice();
      return accumulator + price.getAmount();
    }, 0);

    return new Currency(total);
  }
}
