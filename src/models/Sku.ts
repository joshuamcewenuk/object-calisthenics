/**
 * Representation of a SKU value.
 */
export class Sku {
  private readonly value: string;

  /**
   * Create a new SKU instance.
   * @param value
   */
  constructor(value: string) {
    this.value = value;
  }

  /**
   * Retrieve value for SKU.
   */
  public getValue(): string {
    return this.value;
  }
}
